﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MovieAPI.Models
{
    public class MoviesDbContext:DbContext
    {
        public MoviesDbContext():base("name=MovieDbContext")
        {

        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }
    }
}