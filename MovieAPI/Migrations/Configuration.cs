namespace MovieAPI.Migrations
{
    using MovieAPI.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MovieAPI.Models.MoviesDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MovieAPI.Models.MoviesDbContext context)
        {
                context.Directors.AddOrUpdate(x => x.Id,
                 new Director() { Id = 1, Name = "Steven", Lastname = "Spielberg", Age = 1942 },
                 new Director() { Id = 2, Name = "Martin", Lastname = "Scorsese", Age = 1946 },
                 new Director() { Id = 3, Name = "George", Lastname = "Lucas", Age = 1944 }
                 );

            context.Movies.AddOrUpdate(x => x.Id,
                new Movie()
                {
                    Id = 1,
                    Name = "E.T. the Extra-Terrestrial",
                    Year = 1982,
                    DirectorId = 1,
                    Genre = "SF"
                },
                new Movie()
                {
                    Id = 2,
                    Name = "Ready Player One",
                    Year = 2018,
                    DirectorId = 1,
                    Genre = "SF"
                },
                new Movie()
                {
                    Id = 3,
                    Name = "Goodfellas",
                    Year = 1990,
                    DirectorId = 2,
                    Genre = "Crime"
                },
                new Movie()
                {
                    Id = 4,
                    Name = "Cape Fear",
                    Year = 1991,
                    DirectorId = 2,
                    Genre = "Thriller"
                },
                 new Movie()
                 {
                     Id = 4,
                     Name = "Indiana Jones and the Last Crusade",
                     Year = 1989,
                     DirectorId = 3,
                     Genre = "Action-adventure"
                 }
                );
        }
    }
}
