﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace MovieAPI.Controllers
{
    public class MoviesController : ApiController
    {
        private MoviesDbContext db = new MoviesDbContext();

        private bool MovieExists(int id)
        {
            Movie movie = db.Movies.SingleOrDefault(m => m.Id == id);
            if (movie == null)
            {
                return false;
            }

            return true;
        }

        public IQueryable GetMovies()
        {
            IQueryable<MovieDTO> movies = db.Movies.Include(m => m.Director)
                .Select(b => new MovieDTO()
                {
                    Id = b.Id,
                    Name = b.Name,
                    Genre = b.Genre,
                    DirectorName = (b.Director.Name + " " + b.Director.Lastname)

                });
                
            return movies;
        }

        [ResponseType(typeof(MovieDetailsDTO))]
        public IHttpActionResult GetMovie(int id)
        {
            var movie = db.Movies.Include(m => m.Director)
            .Select(m => new MovieDetailsDTO()
             {
                 Id=m.Id,
                 Name=m.Name,
                 Genre=m.Genre,
                 Year=m.Year,
                 DirectorName=(m.Director.Name+" "+m.Director.Lastname)
                
             }
            ).SingleOrDefault(m => m.Id == id);
                
            if (movie == null)
            {
                return NotFound();
            }

            return Ok(movie);
        }

        [ResponseType(typeof(Movie))]
        public IHttpActionResult PostMovie(Movie movie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Movies.Add(movie);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id=movie.Id},movie);
             
        }

        [ResponseType(typeof(Movie))]
        public IHttpActionResult PutMovie(int id, Movie movie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != movie.Id)
            {
                return BadRequest();
            }

            db.Entry(movie).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(movie);

        }
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteMovie(int id)
        {
            Movie movie = db.Movies.Find(id);

            if (movie == null)
            {
                return NotFound();
            }

            db.Movies.Remove(movie);
            db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
