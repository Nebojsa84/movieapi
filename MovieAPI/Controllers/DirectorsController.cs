﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace MovieAPI.Controllers
{
    public class DirectorsController : ApiController
    {
        private MoviesDbContext db = new MoviesDbContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DirectorExists(int id)
        {
            var dir = db.Directors.FirstOrDefault(d => d.Id == id);

            return (dir == null);
            
        }

        public IQueryable<Director> GetDirectors()
        {
            return db.Directors;
        }

        [ResponseType(typeof(Director))]
        public IHttpActionResult GetDirector(int id)
        {
            Director director = db.Directors.SingleOrDefault(d => d.Id == id);
            if (director == null)
            {
                return NotFound();

            }

            return Ok(director);
        }

        [ResponseType(typeof(Director))]
        public IHttpActionResult PostDirector(Director director)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Directors.Add(director);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { director.Id }, director);
        }

        [ResponseType(typeof(Director))]
        public IHttpActionResult PutDirector(int id,Director director)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(id != director.Id)
            {
                return BadRequest();
            }

            db.Entry(director).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (DirectorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
                
            }

            return Ok(director);

        }
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteDirector(int id)
        {
            Director director = db.Directors.FirstOrDefault(d => d.Id == id);

            if (director == null)
            {
                return NotFound();
            }

            db.Directors.Remove(director);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

    }
}
