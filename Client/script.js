$(document).ready(function(){

	var host= "http://localhost:"
	var port="49544"
	var movieEndpoint="/api/movies/"
	var directorEndpoint="/api/Directors/"

	$("body").on("click", "#btnDelete", deleteMovie);
	$("body").on("click", "#btnEdit", editMovie);



	$("#movie-btn").click(function(){

		$.ajax({
			url:host+port+movieEndpoint,
			type:"get"
		})
		.done(displayMovies);

	});


	$("#director-btn").click(function(){

		$.ajax({
			url:host+port+directorEndpoint,
			type:"get"
		})
		.done(displayDirectors);

	});


	function displayMovies(data,status){

	    console.log(data);

	    var $container = $("#content");
		$container.empty(); 
	    
	    var table = $("<table></table>").addClass("table table-bordered");
	    var header = $("<tr><th>Id</th><th>Name</th><th>Genre</th><th>Director name</th></tr>");
	    table.append(header);

	    for (i=0; i<data.length; i++){
				
			var displayData = "<tr><td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>"+data[i].Genre +"</td><td>"+data[i].DirectorName +"</td>";

			var stringId = data[i].Id.toString();

			var displayDelete = "<td><button class='btn btn-primary' id=btnDelete name=" + stringId + ">Delete</button></td>";
			var displayEdit = "<td><button class='btn btn-btn-warning' id=btnEdit name=" + stringId + ">Edit</button></td>"

			displayData += displayDelete + displayEdit + "</tr>";  
			table.append(displayData);
				
		}

		$container.append(table);

	};


	function displayDirectors(data,status){

		console.log(data);

	    var $container = $("#content");
		$container.empty(); 

		var table = $("<table></table>").addClass("table table-bordered ");
	    var header = $("<tr><th>Id</th><th>Name</th><th>Lastname</th><th>Age</th></tr>");
	    table.append(header);

	    for (i=0; i<data.length; i++){
				
			var displayData = "<tr><td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>"+data[i].Lastname +"</td><td>"+data[i].Age +"</td>";

			var stringId = data[i].Id.toString();

			var displayDelete = "<td><button class='btn btn-warning' id=btnDelete name=" + stringId + ">Delete</button></td>";
			var displayEdit = "<td><button class='btn btn-primary' id=btnEdit name=" + stringId + ">Edit</button></td>"

			displayData += displayDelete + displayEdit + "</tr>";  
			table.append(displayData);
				
		}

		$container.append(table);

	};



	function deleteMovie() {
		
		var deleteID = this.name;
		
		$.ajax({
			url: host + port + movieEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Greska!");
		});

	};


	function editMovie () {
		
		var editID = this.name;
		
		$.ajax({
			url: host + port + movieEndpoint + editID.toString(),
			type: "GET",
		})
		.done(function(data, status){
			console.log(data);
			movieEdit(data,status);
		})
		.fail(function(data, status){
			alert("Greska!");
		});

	};


	function movieEdit(data, status){

		
		 var $container = $("#content");
		 $container.empty(); 
		 		
		 var form=$("<form></form>").addClass("form-group");

		 var nameInput=$("<input type='text' value='"+ data.Name +"' /><br/>");
		 var genreInput=$("<input type='text'value='"+ data.Genre +"' /><br/>");
		 var yearInput=$("<input type='number' value="+ data.Year +" /><br/>");
		 var directorInput=$("<input type='text' value="+ data.DirectorName +" /><br/>");
		 var submitBtn=$("<input type='submit' value='Save changes' /><br/>")

		 form.append(nameInput,genreInput,yearInput,submitBtn);
		 $container.append(form);


	}

	function refreshTable() {

		$("#movie-btn").trigger("click");

	};

});